package com.arghanoa.cube;

import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.*;
import static android.opengl.Matrix.*;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;



/**
 * Created by andrea on 27/04/16.
 */
public class MyGLRenderer implements Renderer {
    private int program;

    private final float[] modelMatrix = new float[16];

    private static final String U_MATRIX = "u_Matrix";
    private final float[] projectionMatrix = new float[16];
    private int uMatrixLocation;



    private final Context context;

    private static final int BYTES_PER_FLOAT = 4;


//    private static final String U_COLOR = "u_Color";
    private static final String A_COLOR = "a_Color";
    private int aColorLocation;
    private static final String A_POSITION = "a_Position";
    private int aPositionLocation;
    private static final int COLOR_COMPONENT_COUNT = 4;
    private static final int POSITION_COMPONENT_COUNT = 3;
    private static final int STRIDE = (POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT) * BYTES_PER_FLOAT;

    private FloatBuffer mVertexBuffer;

    private ByteBuffer  mIndexBuffer;

    private float vertices[] = {
            -1.0f, -1.0f, -1.0f, 0.0f,  1.0f,  0.0f,  1.0f,
            1.0f, -1.0f, -1.0f,0.0f,  1.0f,  0.0f,  1.0f,
            1.0f,  1.0f, -1.0f,1.0f,  0.5f,  0.0f,  1.0f,
            -1.0f, 1.0f, -1.0f,1.0f,  0.5f,  0.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,1.0f,  0.0f,  0.0f,  1.0f,
            1.0f, -1.0f,  1.0f,1.0f,  0.0f,  0.0f,  1.0f,
            1.0f,  1.0f,  1.0f, 0.0f,  0.0f,  1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,1.0f,  0.0f,  1.0f,  1.0f
    };

    private byte indices[] = {
            0, 4, 5, 0, 5, 1,
            1, 5, 6, 1, 6, 2,
            2, 6, 7, 2, 7, 3,
            3, 7, 4, 3, 4, 0,
            4, 7, 6, 4, 6, 5,
            3, 0, 1, 3, 1, 2
    };


    public MyGLRenderer(Context context) {
        super();
        mVertexBuffer = ByteBuffer.allocateDirect(vertices.length * BYTES_PER_FLOAT).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mVertexBuffer.put(vertices);
        mIndexBuffer=ByteBuffer.allocateDirect(indices.length).order(ByteOrder.nativeOrder());
        mIndexBuffer.put(indices);
        this.context=context;
    }




    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //(R,G,B,Trasparency)
        String vertexShaderSource = TextResourceReader.readTextFileFromResource(context, R.raw.simple_vertex_shader);
        String fragmentShaderSource = TextResourceReader.readTextFileFromResource(context, R.raw.simple_fragment_shader);
        int vertexShader = ShaderHelper.compileVertexShader(vertexShaderSource);
        int fragmentShader = ShaderHelper.compileFragmentShader(fragmentShaderSource);
        program = ShaderHelper.linkProgram(vertexShader, fragmentShader);
        glUseProgram(program);
        //uColorLocation = glGetUniformLocation(program, U_COLOR);

        aColorLocation = glGetAttribLocation(program, A_COLOR);
        aPositionLocation = glGetAttribLocation(program, A_POSITION);

        mVertexBuffer.position(0);
        //glVertexAttribPointer(aPositionLocation, POSITION_COMPONENT_COUNT, GL_FLOAT, false, 0, vertexData);
        glVertexAttribPointer(aPositionLocation, POSITION_COMPONENT_COUNT, GL_FLOAT, false, STRIDE, mVertexBuffer);
        glEnableVertexAttribArray(aPositionLocation);

        mVertexBuffer.position(POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(aColorLocation, COLOR_COMPONENT_COUNT, GL_FLOAT, false, STRIDE, mVertexBuffer); //stride quanti byte ci sono tra due colori
        glEnableVertexAttribArray(aColorLocation);

    }

    @Override
    //public void onSurfaceChanged(GL10 glUnused, int i, int il)
    public void onSurfaceChanged(GL10 gl10, int width, int height)
    {
        glViewport(0,0,width,height);
        final float aspectRatio = width > height ?
                (float) width / (float) height :
                (float) height / (float) width;
        if (width > height) {
// Landscape
            orthoM(projectionMatrix, 0, -aspectRatio, aspectRatio, -1f, 1f, -1f, 1f);
        } else {
// Portrait or square
            orthoM(projectionMatrix, 0, -1f, 1f, -aspectRatio, aspectRatio, -1f, 1f);
        }

    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        //pulisce la superficie di rendering
        glClear(GL_COLOR_BUFFER_BIT);

        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_BYTE, mIndexBuffer);

    }
}
